#[derive(Debug)]
struct Rectangle{
    width: i32,
    height: i32,
}

#[derive(Debug)]
struct Triangle{
    height: f64,
    base: f64,
}

fn main() {

    let rect = Rectangle{
        width: 30,
        height: 50,
    };

    let tria = Triangle{
        height: 40.0,
        base: 20.0,
    };
    println!("The value of rect is {:?}", rect);
    println!("The value of tria is {:?}", tria);
    println!("The area of the rectangle is {}", rect_area(&rect));
    println!("The area of the triangle is {}", tria_area(&tria));
}


fn rect_area(rectangle: &Rectangle) -> i32{
    rectangle.width * rectangle.height
}

fn tria_area(triangle: &Triangle) -> f64
{
    let a = triangle.base * 0.5;
    a * triangle.height
}
