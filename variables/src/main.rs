use std::io;

fn main() {
    const MAX_POINTS: u32 = 100_000; //Declaraction of constants

    let y = 10; // Declaraction of a immutable variable

    let mut x = 5; // Declaraction of a mutable variable

    println!("The variable x contains {}", x);
    x = 6; //this will throw an error if x is an immutable variable (without mut keyword)
    println!("The value of x is {}", x);
    println!("The value of the constant is {}", MAX_POINTS);

    //variable shadowing
    println!("The original value of y {}", y);
    //Y value manupulations
    let y = y + 1;
    let y = y + 10;
    println!("The value of y is {}", y);

    //Floating point types
    let a = 2.021; // f64

    let b: f32 = 3.0; //f32

    println!("The value of a contains: {}", a);
    println!("The value of b contains: {}", b);

    //Arithmetic Operations
    
    let sum  = 5 + 10; //addition operation

    let difference = 95.5 - 4.3; //subtraction operation

    let product = 4 * 30; //multiplication operation

    let qoutient = 54.3 / 3.2; //division operation

    let remainder = 54.3 % 3.2; //modulo operation

    println!("The value of sum is {}", sum);
    println!("The value of difference is {}", difference);
    println!("The value of product is {}", product);
    println!("The value of qoutient is {}", qoutient);
    println!("The value of remainder is {}", remainder);

    // The Boolean Type
    let t = true; //letting rust detect that this a boolean variable

    let f:bool = false; //explicity specifying the variable as a boolean variable

    println!("The value of t is {}", t);

    println!("The value of f is {}", f);

    // The Character Type
    let c = 'z';

    println!("The value of c is {}", c);

    // Compound Types
    // The Tuple Types
    let tup: (i32, f64, u8) = (500, 6.4, 1);

    let (m, n, o) = tup;

    println!("The value of m is {}", m);
    println!("The value of n is {}", n);
    println!("The value of o is {}", o);


    //The Array Type
    let _p = [1, 2, 3, 4, 5];
    let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
    let _q: [i32; 5] = [1, 2, 3, 4, 5];

    let o = [3; 8]; //create an array with 8 threes

    let first = o[0];
    let second = months[1];

    println!("The first value of the o array {}", first);
    println!("The second value of the second array {}", second);

    println!("Please enter the numerical value of birth month");

    let mut index  = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index
        .trim()
        .parse()
        .expect("Index entered was not a number");

    let index = index - 1;

    let element = months[index];

    println!("The value of the element at index {} is: {}", index, element);

}
