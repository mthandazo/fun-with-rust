fn sum_operation(){
    let a = 4;
    let b = 5;
    println!("The sum of {} and {} is {}", a, b, a + b);
}

fn sum_operation_with_params(x: i32, y: i32)
{
    println!("The sum of {} and {} is {}", x, y, x + y);
}

fn five() -> i32{
    5
}

fn plus_one(x: i32) -> i32{
    x + 1
}

fn shift_one(y: i32) -> i32{
    y << 1
}

fn shift_right(y: i32) -> i32{
    y >> 1
}

fn loop_with_return_value()
{
    let mut counter = 0;

    let result = loop{
        counter += 1;

        if counter == 10{
            break counter * 2;
        }
    };
    println!("The result of this function is {}", result);
}

fn while_cond_loop(max: i32){
    let mut number = max;

    while number != 0{
        println!("{}!", number);
        number -= 1;
    }
    println!("LIFTOFF!!!");
}

fn loop_cond_while(){
    let a = [1, 2, 3, 4, 5, 6];
    let mut index = 0;

    while index < 5{
        println!("The value is: {}", a[index]);
        index += 1;
    }
}

fn for_iter_coll(){
    let a = [10, 20, 30, 40, 50, 60];

    for element in a.iter(){
        println!("The value is: {}", element);
    }
}

fn for_seq(){
    for number in (1..4).rev(){
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}

fn main() {
    println!("Hello Functions!");
    sum_operation();
    sum_operation_with_params(-100, 300);
    for_seq();
    for_iter_coll();
    loop_cond_while();
    while_cond_loop(12);
    loop_with_return_value();
    let _x = 5;

    let y = {
        let x = 3;
        x + 1
    };

    println!("The value of y is {}", y);

    let z = five();

    println!("The value of z is {}", z);

    let m = plus_one(10);

    let n = shift_one(10);

    let o = shift_right(10);

    println!("The value of m is {}", m);
    println!("The value of n is {}", n);
    println!("The value of o is {}", o);
}
