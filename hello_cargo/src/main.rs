fn main() {
    const MAX_POINTS: u32 = 100_000; //declaration of a constant
    let mut x = 5; // declaration of a mutable variable
    let y = 10; //declaraction of a immutable variable

    println!("The value of x is: {}", x);

    let mut x = x + 1;
    println!("The value of x is {}", x);

    println!("The value of y is {}", y);
    x = 6;
    println!("The value of x is {}", x);

    println!("The value of the constant is {}", MAX_POINTS);

    for number in (1..4).rev()
    {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
