#[derive(Debug)]
struct Rectangle{
    width: i32,
    height: i32,
}

impl Rectangle{
    fn area(&self) -> i32{
        self.width * self.height
    }
}

#[derive(Debug)]
struct Triangle{
    height: f64,
    base: f64,
}

impl Triangle{
    fn area(&self) -> f64{
        let a = self.base * 0.5;
        self.height * a
    }
}


fn main() {
    let rect = Rectangle{
        width: 30,
        height:50,
    };

    let tria = Triangle{
        height:40.0,
        base:20.0,
    };


    println!("The area of the rectangle is {}", rect.area());
    println!("The area of the triangle is {}", tria.area());
}
